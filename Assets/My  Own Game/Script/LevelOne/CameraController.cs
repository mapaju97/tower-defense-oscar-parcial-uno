﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {
    
    [SerializeField] private float speed = 30.0f;
    [SerializeField] private float borderThickness = 10f;
    [SerializeField] private Transform cameraPos;
   

    // Update is called once per frame
    void Update()
    {
        if (GameM.gameEnded)
        {
            this.enabled=false;
            return;
        }
        if (Input.mousePosition.y <=Screen.height-borderThickness && cameraPos.transform.position.z > -16)
        {
            
            transform.Translate(Vector3.back*speed*Time.deltaTime,Space.World);
        }

        if ( Input.mousePosition.y >=  borderThickness && cameraPos.transform.position.z < 10)
        {

            transform.Translate(Vector3.forward * speed * Time.deltaTime, Space.World);
        }

        if ( Input.mousePosition.x >= Screen.width - borderThickness && cameraPos.transform.position.x < 9)
        {

            transform.Translate(Vector3.right * speed * Time.deltaTime, Space.World);
        }

        if ( Input.mousePosition.x <=  borderThickness && cameraPos.transform.position.x > -20)
        {

            transform.Translate(Vector3.left * speed * Time.deltaTime, Space.World);
        }
    }


     
    
}

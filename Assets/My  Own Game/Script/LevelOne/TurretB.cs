﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class TurretB {

    public GameObject turretPrefab;
    public int cost;
}

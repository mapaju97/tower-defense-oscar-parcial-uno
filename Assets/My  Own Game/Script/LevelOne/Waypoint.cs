﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Waypoint : MonoBehaviour {
    public static Transform[] areaPoints;

    private void Awake()
    {
        areaPoints = new Transform[transform.childCount];
        for(int i =0; i<areaPoints.Length; i++)
        {
            areaPoints[i] = transform.GetChild(i);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Enemy : MonoBehaviour {

    public float speed = 10f;
    private Transform goal;
    private int wavepointIndex = 0;
    [SerializeField] float health = 100;
    [SerializeField] GameObject particlesPrefab;
    public GameObject particleposition;
   
    public int moneyEarn = 4;
    [SerializeField] Image healthEnemy;

    private void Start()
    {
        goal = Waypoint.areaPoints[0];
       
    }
    public void TakeDamage(float amount)
    {
        amount = 5f;
        health -= amount;
        healthEnemy.fillAmount = health/100f;
        if (health <= 0)
        {
            Die();
        }
    }

    void Die()
    {
        PlayerCoin.money += 1;
        Debug.Log("i am doing something");
        Destroy(gameObject);
    }
    private void Update()
    {
        Vector3 direction = goal.position - transform.position;
        transform.Translate(direction.normalized*speed*Time.deltaTime,Space.World);

        if (Vector3.Distance(transform.position, goal.position) <= .4)
        {
            GetNextWaypoint();
        }
    }

    void GetNextWaypoint()
    {
        if (wavepointIndex >= Waypoint.areaPoints.Length - 1)
        {
            EndPath();
            return;

        }
        wavepointIndex++;
        goal = Waypoint.areaPoints[wavepointIndex];
    }

    void EndPath()
    {
        PlayerCoin.lives--;
        Destroy(gameObject);
        Instantiate(particlesPrefab, particleposition.transform.position, Quaternion.identity);
       
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FlyingMenu : MonoBehaviour {
    public Transform currentPosition, secondPosition, mainMenuposition;
    public GameObject panelOne;
    public GameObject panelTwo;
    // Use this for initialization
    void Start () {
        panelTwo.gameObject.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
        transform.localPosition = Vector3.Lerp(transform.localPosition, currentPosition.position, 0.3f);
        transform.localRotation = Quaternion.Slerp(transform.localRotation, currentPosition.rotation, 0.3f);
        
    }

    public void LevelSelect()
    {
        currentPosition = secondPosition;
        panelOne.gameObject.SetActive(false);
        panelTwo.gameObject.SetActive(true);
    }

    public void Back()
    {
        currentPosition= mainMenuposition;
        panelTwo.gameObject.SetActive(false);
        panelOne.gameObject.SetActive(true);
    }

    public void PlayLevelOne()
    {
        SceneManager.LoadScene("LevelOne");
    }
}

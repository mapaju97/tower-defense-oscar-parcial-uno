﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;


public class WaveSpawner : MonoBehaviour {

   // [Serializable]
   // public struct Waves
   // {
       // public Enemy
    //}




    [SerializeField]Transform enemyPrefab;
    [SerializeField] Transform SpawnLocation;
    [SerializeField] float timeBetweenWaves=20.0f;
    [SerializeField] float countdown = 20.0f;
    [SerializeField] int numberOfWaves = 1;
   [SerializeField] Text countDownText;
    [SerializeField] GameObject gameWon;



    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (countdown <= 0)
        {
            StartCoroutine(SpawnWave());
            countdown = timeBetweenWaves;

        }
        countdown -= Time.deltaTime;
        countdown = Mathf.Clamp(countdown, 0f, Mathf.Infinity);
        countDownText.text = string.Format("{0:00.00}", countdown);
    }

    
    IEnumerator  SpawnWave()
    {

        numberOfWaves++;
        if (numberOfWaves <= 10)
        {
            PlayerCoin.hordes++;
            print(numberOfWaves);
            for (int i = 0; i < numberOfWaves; i++)
            {
                SpawnEnemy();
                yield return new WaitForSeconds(1.0f);
            }

           
        }

        if (numberOfWaves == 11) 
       {
           
            gameWon.SetActive(true);

            Time.timeScale = 0;
        }
    }

    public void SpawnEnemy()
    { 
        Instantiate(enemyPrefab, SpawnLocation.position, Quaternion.identity);
    }

    public void StartWave()
    {
       
       
    }

    

}

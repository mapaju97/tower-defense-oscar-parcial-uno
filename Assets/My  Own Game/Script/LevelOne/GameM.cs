﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameM : MonoBehaviour {

    public static bool gameEnded;
    [SerializeField] GameObject gameOver;
   
    
    // Update is called once per frame

    private void Start()
    {
        gameEnded = false;
    }
    void Update () {

        if (gameEnded)
            return; 

        if (PlayerCoin.lives <= 0)
        {
            EndGame();
        }
	}

    public void EndGame()
    {
        gameEnded = true;
        gameOver.SetActive(true);

        Time.timeScale = 0;
    }

   // public void WinGame()
    //{
      
      
        
    //}
  
}

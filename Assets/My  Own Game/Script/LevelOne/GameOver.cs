﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameOver : MonoBehaviour {

    public Text hordesText;

    private void OnEnable()
    {
        hordesText.text = PlayerCoin.hordes.ToString();
    }

    public void Retry()
    {
        SceneManager.LoadScene("LevelOne");

        Time.timeScale = 1;
    }
    public void Menu()
    {
        SceneManager.LoadScene("MainMenu");

        Time.timeScale = 1;
    }
}


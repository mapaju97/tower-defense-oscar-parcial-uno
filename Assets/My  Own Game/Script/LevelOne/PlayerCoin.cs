﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCoin : MonoBehaviour {

    public static int money;
    [SerializeField] int defaultMoney=20;
    public static int lives;
    [SerializeField] int startlives = 10;
    public static int hordes;

    private void Start()
    {
        money = defaultMoney;
        lives = startlives;
        hordes = 0;
    }


    
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class GroundPlace : MonoBehaviour {
   [SerializeField] public Color colorOverlap;
    [SerializeField] public Color noMoneyColor;
     private Renderer rend;
     private Color startColor;
    public GameObject turrent;
    BuildManager buildmanager;
    public Vector3 positionO;

    void Start()
    {
        rend = GetComponent<Renderer>();
        startColor = rend.material.color;

        buildmanager = BuildManager.instance;
    }

    private void OnMouseDown()
    {
        if (!buildmanager.CanBuild)
            return;


        if (turrent != null)
        {
            Debug.Log("We cannot build there");
            return;

        }

        buildmanager.BuildTurretIn(this);
    }


    void OnMouseEnter()
    {
        if (EventSystem.current.IsPointerOverGameObject())
            return;


        if (!buildmanager.CanBuild)
            return;

        if (buildmanager.HasMoney)
        {
            rend.material.color = colorOverlap;
        }
        else
        {
            rend.material.color = noMoneyColor;
        }

        
           
    }

    void OnMouseExit()
    {
        rend.material.color = startColor;
    }

    public Vector3 GetBuilPosition()
    {
        return transform.position + positionO;
    }

    
}

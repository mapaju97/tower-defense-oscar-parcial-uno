﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Turrent : MonoBehaviour {

    [SerializeField] private float range = 15f;
    [SerializeField] private Transform target;
    [SerializeField] private Transform rotationGun;
    [SerializeField] private float speedrotation = 5.0f;
    [SerializeField] private float timeToShot=2.0f;
    [SerializeField] private float shotCount=0f;
    [SerializeField] private GameObject bulletPrefab;
    [SerializeField] private Transform bulletStartPosition;



   // Use this for initialization
   void Start () {
        InvokeRepeating("Target", 0, 0.5f);
        
    }
	
	// Update is called once per frame
	void Update () {

        if (target == null)
        {
            return;
        }

        //transform.LookAt(target); intente con este pero se movia en x y y z 
         //asi que busque un tutorial y lo hice con más lineas :'(
    
            Vector3 direction = target.position - transform.position;
        Quaternion objetiveRotation = Quaternion.LookRotation(direction);
        Vector3 rotation = Quaternion.Lerp(rotationGun.rotation,objetiveRotation,Time.deltaTime*speedrotation).eulerAngles;
          rotationGun.rotation=Quaternion.Euler(0,rotation.y,0);

        if (shotCount <= 0f)
        {
            Shooter();

            shotCount = 2f / timeToShot;
        }

        shotCount -= Time.deltaTime;
           
		
	}

    void Shooter()
    {
        GameObject bulletClone= (GameObject) Instantiate(bulletPrefab, bulletStartPosition.position, Quaternion.identity);
        Bullet bullet = bulletClone.GetComponent<Bullet>();

        if(bullet != null)
        {
            bullet.Search(target);

            
        }
    }

    void Target()
    {
        GameObject[] enemies = GameObject.FindGameObjectsWithTag("enemy");
        float shortDistance = Mathf.Infinity;
        GameObject nearenemy = null;


        foreach (GameObject enemy in enemies)
        {
            float distanceBetwenPlayerandEnemy = Vector3.Distance(transform.position, enemy.transform.position);
            if (distanceBetwenPlayerandEnemy < shortDistance)
            {
                shortDistance = distanceBetwenPlayerandEnemy;
                nearenemy = enemy;
            }


        }

        if(nearenemy!=null && shortDistance<= range)
        {
            target = nearenemy.transform;
        }
        else
        {
            target = null;
        }
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position,range);
    }
}

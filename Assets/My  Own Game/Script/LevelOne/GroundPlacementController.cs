﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundPlacementController : MonoBehaviour {
    [SerializeField] private GameObject placeObjectsPrefab;
    [SerializeField] private KeyCode newObjecttoKey = KeyCode.A;
    private GameObject currentPlacebleObject;
    private float mouseWheelRotation;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        HandleNewObjectKey();

        if (currentPlacebleObject!=null)
        {
            MovePlacableObjectToMouse();
            RotateFromMouseWheel();
            RoleaseIfClicked();
        }
	}

    private void RoleaseIfClicked()
    {

        if (Input.GetMouseButtonDown(0))
        {
            currentPlacebleObject = null;
        }


    }
    private void RotateFromMouseWheel()
    {

        mouseWheelRotation = Input.mouseScrollDelta.y;
        currentPlacebleObject.transform.Rotate(Vector3.up, mouseWheelRotation * 10f);
    }
    private void HandleNewObjectKey()
    {
        if (Input.GetKeyDown(newObjecttoKey))
        {
            currentPlacebleObject = Instantiate(placeObjectsPrefab);
        }
        else
        {
            Destroy(currentPlacebleObject);
        }
    }


    void MovePlacableObjectToMouse()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hitInfo;

        if(Physics.Raycast(ray, out hitInfo))
        {
            currentPlacebleObject.transform.position = hitInfo.point;
            currentPlacebleObject.transform.rotation = Quaternion.FromToRotation(Vector3.up, hitInfo.normal);
        }
    }
}

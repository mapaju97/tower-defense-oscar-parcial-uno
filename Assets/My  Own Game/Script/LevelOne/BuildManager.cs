﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildManager : MonoBehaviour {
    private TurretB buildingTurrent;
    public GameObject standardTurrentPrefab;

    public static BuildManager instance;

    public bool CanBuild {get { return buildingTurrent != null; }}
    public bool HasMoney { get { return PlayerCoin.money>= buildingTurrent.cost; } }

    void Awake()
    {
        if (instance != null)
        {
            Debug.LogError("More than one BuildManager is in the scene¡");
            return;
        }
        instance = this;
    }
   

   public void SelectTurretToBuild(TurretB turret)
    {
        buildingTurrent = turret;
    }

    public void BuildTurretIn(GroundPlace node)
    {
        if (PlayerCoin.money< buildingTurrent.cost)
        {
            Debug.Log("There is not money");
            return;
        }

        PlayerCoin.money -= buildingTurrent.cost;

     GameObject turret=(GameObject) Instantiate(buildingTurrent.turretPrefab, node.GetBuilPosition(), Quaternion.identity);
        node.turrent = turret;

        Debug.Log("You have just" + PlayerCoin.money);
    }


    // Update is called once per frame
    void Update () {
		
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

    [SerializeField] private Transform target;
    public float speed = 70f;
    public int damage = 40;
    public float explosionRadious = 0f;
    public GameObject explosion;
   

    public void Search(Transform _target)
    {
        target = _target;
    }
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
		if (target == null)
        {
            Destroy(gameObject);
            return;
        }

        Vector3 direction = target.position - transform.position;
        float speedFrame = speed * Time.deltaTime;

        if (direction.magnitude<= speedFrame)
        {
            ImpactTarget();
        }

        transform.Translate(direction.normalized * speedFrame, Space.World);
        transform.LookAt(target);
    }

    //para destruir la balita que sigue al enemigo
    void ImpactTarget()
    {
       GameObject effectClone= (GameObject) Instantiate(explosion, transform.position, transform.rotation);
        Destroy(effectClone,3f);

        if (explosionRadious > 0f)
        {
            Explode();
        }
        else
        
        {
           
            Damage(target);
        }

        Destroy(gameObject);
    }

    void Damage(Transform enemy)
    {
        Enemy enem = enemy.GetComponent<Enemy>();

        if (enem != null)
        {
            enem.TakeDamage(damage);
        }
     
        
        //Destroy(enemy.gameObject);
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, explosionRadious);
    }
    void Explode()
    {
        Collider[] colliders= Physics.OverlapSphere(transform.position, explosionRadious);
        foreach (Collider collider in colliders)
        {
            if (collider.tag == "enemy")
            {
                Damage(collider.transform);
            }
        }
    }
 

}
